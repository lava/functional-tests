#!/bin/sh

set -e

threshold=$1

[ -z "$threshold" ] && {
    echo "No threshold provided"
    echo "Usage: battery-health-check.sh <battery-threshold>"
    exit 1
}

if [ "$LAVA" = "y" ]; then
    alias test-result='lava-test-case'
    alias test-exception='lava-test-raise'
else
    alias test-result='echo'
    alias test-exception='echo'
fi

if batteries=$(grep -l Battery /sys/class/power_supply/*/type); then
    test-result battery-present --result pass
else
    test-exception "No battery detected; job exit"
fi

if ac=$(grep -l Mains /sys/class/power_supply/*/type); then
    test-result ac-present --result pass
    if [ $(cat $(dirname $ac)/online) -eq 1 ]; then
        test-result ac-online --result pass
    else
        test-result ac-online --result fail
    fi
else
    echo "Could not find any info about AC; check kernel configuration."
    test-result ac-present --result skip
fi

for battery in $batteries; do
    dir=$(basename $(dirname $battery))

    # skip batteries that do not power the whole system
    if [ -f "$(dirname $battery)/scope" ] && [ $(cat $(dirname $battery)/scope) != "System" ] ; then
        test-result $dir-capacity --result skip
        continue
    fi

    # report battery status
    if status=$(cat $(dirname $battery)/status); then
        echo "$dir status: $status"
        test-result $dir-status --result pass
    else
        test-result $dir-status --result fail
    fi

    # test battery capacity
    if cap=$(cat $(dirname $battery)/capacity); then
        if [ "$cap" -gt "$threshold" ]; then
            test-result $dir-capacity --result pass --measurement "$cap"
        else
            test-exception "$dir capacity under $threshold% ($cap%); job exit"
        fi
    else
        test-result $dir-capacity --result fail
    fi
done