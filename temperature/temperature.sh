#!/bin/sh

set -e

while [ $# -gt 0 ] ; do
  case $1 in
    -t | --threshold) threshold="$2" ;;
  esac
  shift
done

[ -z "$threshold" ] && {
    echo "No threshold provided"
    echo "Usage: $(basename "$0") <temperature-threshold>"
    exit 1
}

if [ "$LAVA" = "y" ]; then
    alias test-result='lava-test-case'
    alias test-exception='lava-test-raise'
else
    alias test-result='echo'
    alias test-exception='echo'
fi

if zones=$(find /sys/class/thermal/thermal_zone*); then
    test-result temperature-sensors-present --result pass
else
    test-result temperature-sensors-present --result fail
    echo "No temperature sensors detected"
    exit 1
fi

for zone in $zones; do
    temp=$(cat "$zone"/temp) && res='pass' || res='fail'
    if [ $res = 'pass' ] && [ "$temp" -gt "$threshold" ]; then
        test-exception "$(cat "$zone"/type) temperature over $threshold m°C ($temp m°C); job exit"
        exit 1
    fi
    test-result "$(cat "$zone"/type)" --result "$res" --measurement "$temp" --units m°C
done
