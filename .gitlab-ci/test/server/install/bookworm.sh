#!/bin/sh

set -e

if [ "$1" = "setup" ]
then
  set -x
  apt-get update -q
  apt-get install --no-install-recommends --yes ca-certificates gnupg postgresql wget
  wget https://apt.lavasoftware.org/lavasoftware.key.asc
  apt-key add lavasoftware.key.asc
  echo "deb http://apt.lavasoftware.org/daily bookworm main" > /etc/apt/sources.list.d/lava-daily.list
else
  set -x
  apt-get update -q
  service postgresql start
  DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends --yes lava-server
  lava-server manage lava-scheduler &
  lava-server manage lava-publisher &
  gunicorn3 lava_server.wsgi &
  lava-server manage check --deploy
  lava-server manage migrate --no-color
  lava-server manage createsuperuser --username functional --noinput --email a@test.com
  lava-server manage users add nobody --email b@test.com
  lava-server manage users add staffer --email c@test.com --staff
  lava-server manage workers add functional
  lava-server manage workers add disabled --health MAINTENANCE
  lava-server manage device-types add qemu
  lava-server manage device-types list --all
  lava-server manage devices add --device-type qemu --worker functional qemu01
  lava-server manage devices list --all
  lava-server manage devices update --health RETIRED qemu01
  lava-server manage devices list --all
fi
