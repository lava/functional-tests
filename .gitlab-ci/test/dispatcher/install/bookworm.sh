#!/bin/sh

set -e

if [ "$1" = "setup" ]
then
  set -x
  apt-get update -q
  apt-get install --no-install-recommends --yes ca-certificates gnupg wget
  wget https://apt.lavasoftware.org/lavasoftware.key.asc
  apt-key add lavasoftware.key.asc
  echo "deb http://apt.lavasoftware.org/daily bookworm main" > /etc/apt/sources.list.d/lava-daily.list
else
  set -x
  apt-get update -q
  DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends --yes lava-dispatcher
fi
