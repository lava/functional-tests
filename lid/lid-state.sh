#!/bin/sh

if [ "$LAVA" = "y" ]; then
    alias test-result='lava-test-case'
    alias test-exception='lava-test-raise'
else
    alias test-result='echo'
    alias test-exception='echo'
fi

# query lid switch state on all input events
for ev in /dev/input/event*; do
    evtest --query "$ev" EV_SW SW_LID
    if [ $? -eq '10' ]; then
        test-exception "Lid is closed; job exit"
        exit 1
    fi
done

test-result lid-open --result pass
