#!/bin/sh

set -e

if [ "$LAVA" = "y" ]; then
    alias test-result='lava-test-case'
    alias test-exception='lava-test-raise'
else
    alias test-result='echo'
    alias test-exception='echo'
fi

if ! command -v nc >/dev/null; then
    echo "nc could not be found"
    exit 1
fi

set -- "google.com" \
    "kernel.org" \
    "deb.debian.org" \
    "gitlab.collabora.com" \
    "gitlab.freedesktop.org" \
    "images.apertis.org"

count=0
for domain in "$@"; do
    if nc -vz -w 1 "$domain" 443; then
        res=pass
        count=$((count + 1))
    else
        res=fail
    fi
    test-result $domain --result "$res"
done

if [ "$count" -gt 0 ]; then
    test-result network-test --result pass --measurement "$count" --units domains
else
    test-result network-test --result fail --measurement "$count" --units domains
    test-exception "Network not able to connect; job exit"
    exit 1
fi

exit 0
