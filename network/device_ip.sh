#!/bin/sh

set -e

dictionary_device_ip=$1

if [ "$LAVA" = "y" ]; then
	alias test-result='lava-test-case'
	alias test-exception='lava-test-raise'
else
	alias test-result='echo'
	alias test-exception='echo'
fi

if [ -z "$dictionary_device_ip" ]; then
	test-exception "No device IP specified"
	echo "Usage:\t $0 <device_ip>"
	exit 1
fi

device_ips=`/sbin/ifconfig |awk -F" " '$1 ~ /^inet$/ && $2 !~ /^127\.0\.0\.1$/ {print $2}'|tr '\n' ' '`

for ip in $device_ips
do
	if [ "z$ip" = "z$dictionary_device_ip" ]
	then
		test-result network-device-ip --result pass 
		exit 0
	fi
done

test-exception "IP address in device dictionary ($dictionary_device_ip) does not match any address on the device ($device_ips); job exit"
