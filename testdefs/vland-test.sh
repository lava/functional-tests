#!/bin/sh

set -e

export DEBIAN_FRONTEND=noninteractive
unset LANG
unset LANGUAGE

if [ -z `which lava-test-case || true` ]; then
    echo "lava-test-case not found in $PATH"
    exit 1
fi

. ./testdefs/lava-common

# VLAN name we're looking for
VLAN=$1

echo "Told to look for VLAN named '$VLAN'"

# Find the ethernet interface we're meant to be testing with

# 1. Grab the LAVA interface name
echo "Running lava-vland-names"
lava-vland-names
LAVA_IFACE=`lava-vland-names | grep "${VLAN}" | cut -d, -f2 | tr -d '\\n' || true`
if [ -z "${LAVA_IFACE}" ]; then
    lava-test-case find-interface --result fail
    lava-vland-names
    echo ${VLAN}
    lava-test-raise "Unable to identify interface name for '${VLAN}'"
fi
echo "Found LAVA interface '${LAVA_IFACE}' for vlan '${VLAN}'"
lava-test-case find-interface --result pass

# 2. Now find the MAC for that interface
echo "Running lava-vland-self"
lava-vland-self
MAC=`lava-vland-self | grep "${LAVA_IFACE}" | cut -d, -f2 | tr -d '\\n' || true`
if [ -z "${MAC}" ]; then
    lava-test-case find-mac-address --result fail
    lava-vland-self
    echo ${LAVA_IFACE}
    lava-test-raise "Unable to identify mac address for '${LAVA_IFACE}'"
fi
echo "Found MAC address '${MAC}' for interface '${LAVA_IFACE}'"
lava-test-case find-mac-address --result pass

# 3. Now find the eth name for that MAC
echo "Running ip -o link"
ip -o link
IFACE=`ip -o link | grep "${MAC}" | cut -d: -f2 || true`
if [ -z "${IFACE}" ]; then
    lava-test-case find-interface-name --result fail
    ip -o link
    echo ${MAC}
    lava-test-raise "Unable to find interface name for '${MAC}'"
fi
echo "Found interface name '${IFACE}' for mac address '${MAC}'"
lava-test-case find-interface-name --result pass

# Which role are we?
ROLE=$(lava-role)

# Set IP addresses based on that
case ${ROLE} in
    bbb1)
        THEIRIP=192.168.0.2
        # include a default gateway
        MYIP=192.168.0.1/24
    ;;
    bbb2)
        THEIRIP=192.168.0.1
        # include a default gateway
        MYIP=192.168.0.2/24
    ;;
    *)
        lava-test-raise "$0: Got unexpected LAVA role $ROLE, bailing"
        exit 1
	;;
esac

# Let's bring that interface up and set the address
echo "Running ip address add $MYIP dev ${IFACE}"
lava-test-case "set-ip-address" --shell ip address add $MYIP dev ${IFACE}
echo "Waiting to allow for the interface to be configured"
sleep 10
echo "Running ip link set dev ${IFACE} up"
lava-test-case "raise-interface" --shell ip link set dev ${IFACE} up
echo "Waiting to allow for the interface to come up"
sleep 10

echo "Debug: ip a"
ip a

echo "Debug: ip route"
ip route

lava-sync wait-interface

# First, bbb1 will ping for a few seconds and validate. Wait for bbb2 to come up
if [ $ROLE = "bbb1" ]; then

    echo "$ROLE: pinging ${THEIRIP}"
    command "ping_bbb2" "ping -W5 -c10 ${THEIRIP}"
    echo "$ROLE: pinging finished"
    lava-sync bbb1-finished

    echo "$ROLE: waiting as a responder"
    # Wait for other end to finish pinging
    lava-sync bbb2-finished
    echo "$ROLE: responder finished"

    echo "Debug: ip a"
    ip a

    echo "Debug: ip route"
    ip route

else # bbb2

    echo "$ROLE: waiting as a responder"
    # Wait for other end to finish pinging
    lava-sync bbb1-finished
    echo "$ROLE: responder finished"

    echo "$ROLE: pinging"
    command "ping_bbb1" "ping -W5 -c10 ${THEIRIP}"
    echo "$ROLE: pinging finished"
    lava-sync bbb2-finished

    echo "Debug: ip a"
    ip a

    echo "Debug: ip route"
    ip route
fi

# All done
echo "$ROLE exiting successfully"
exit 0
