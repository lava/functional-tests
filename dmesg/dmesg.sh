#!/bin/sh

set -e

if [ "$LAVA" = "y" ]; then
    alias test-result='lava-test-case'
    alias test-exception='lava-test-raise'
else
    alias test-result='echo'
    alias test-exception='echo'
fi

for level in crit alert emerg; do
    dmesg --level=$level --notime -x -k > dmesg.$level
    test -s dmesg.$level && res=fail || res=pass
    count=$(cat dmesg.$level | wc -l)
    cat dmesg.$level
    if [ "$count" -eq 0 ]; then
        test-result \
            $level \
            --result $res \
            --measurement $count \
            --units lines
    else
        test-exception "Kernel reported $count $level errors; job exit"
        exit 1
    fi
done

exit 0
