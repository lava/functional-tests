#!/usr/bin/python3


import os
import sys
import struct
import shutil
import subprocess


# Get all the environment variables.
try:
    ROOTFS_IMG = sys.argv[1]
    ROOTFS_EXT4 = sys.argv[2]
    BOOT_IMG = sys.argv[3]
    OVERLAY = sys.argv[4]
except KeyError:
    raise

if not shutil.which('lava-test-case'):
    raise Exception("Command 'lava-test-case' not found")
if not shutil.which('lava-test-raise'):
    raise Exception("Command 'lava-test-raise' not found")


def run_command(cmd_list, test_case_name):
    try:
        if shutil.which(cmd_list[0]):
            subprocess.run(cmd_list)
            subprocess.run(['lava-test-case', test_case_name, '--result',
                            'pass'])
        else:
            raise Exception("Command '%s' not found" % cmd_list[0])
    except subprocess.CalledProcessError:
        subprocess.run(['lava-test-raise', test_case_name])


def resize():
    with open(OVERLAY, 'rb') as f:
        f.seek(-4, 2)
        overlay_size = int((struct.unpack('I', f.read(4))[0]) / 1024)
    run_command(['simg2img', ROOTFS_IMG, ROOTFS_EXT4], 'simg2img-rootfs')
    run_command(['e2fsck', '-y', '-f', ROOTFS_EXT4], 'run-e2fsck')
    current_size = int(os.path.getsize(ROOTFS_EXT4) / 1024)
    final_size = current_size + overlay_size + 32768
    run_command(['resize2fs', ROOTFS_EXT4, str(final_size) + 'K'], 'resize')
    run_command(['img2simg', ROOTFS_EXT4, ROOTFS_IMG], 'img2simg-rootfs')


if __name__ == '__main__':
    resize()
